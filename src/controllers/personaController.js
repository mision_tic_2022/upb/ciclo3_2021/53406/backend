//const mysql = require('mysql');
const key_database = require('../database/key_database');
const Persona = require('../models/persona');

let obj_1 = {
    "nombre": "Andrés",
    "apellido": "Quintero"
}
let obj_2 = {
    "nombre": "Ana maria",
    "apellido": "Hernán"
}
let obj_3 = {
    "nombre": "Alexander",
    "apellido": "Gil"
}

var personas = [obj_1, obj_2, obj_3];

class PersonaController {
    constructor() {

    }

    //ACCIONES / MÉTODOS QUE PROCESAN SOLICITUDES HTTP


    getPersonas(req, res) {
        Persona.find((error, data) => {
            if (error) {
                res.status(500).send();
            } else {
                res.status(200).json(data);
            }
        });
    }

    getPersonaApellido(req, res) {
        let apellido = req.params.apellido;
        Persona.findOne({apellido: apellido}, (error, data)=>{
            if(error){
                res.status(500).send();
            }else{
                res.status(200).json(data);
            }
        })
        /*
        let apellido = req.params.apellido;
        console.log(apellido);
        let persona = personas.filter(obj => (obj.apellido == apellido));
        res.status(200).json(persona);
        */
    }

    crearPersona(req, res) {
        Persona.create(req.body, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(201).json(data);
            }
        });
    }

    actualizarPersona(req, res) {
        let { id, nombre, apellido, edad } = req.body;
        let obj = {
            nombre,
            apellido,
            edad
        }
        /*
        Recibe tres parámetros: el id del documento a actualizar, el objeto que sobreescribirá 
        y una función flecha
        */
        Persona.findByIdAndUpdate(id, { $set: obj }, (error, data) => {
            if (error) {
                res.status(500).send();
            } else {
                res.status(200).json(data);
            }
        });
    }

    eliminarPersona(req, res) {
        //Capturar el id del cuerpo de la petición
        let { id } = req.body;
        //Eliminar el documento que corresponda con el id recibido como parámetro
        Persona.findByIdAndRemove(id, (error, data) => {
            if (error) {
                res.status(500).json({ error });
            } else {
                res.status(200).json(data);
            }
        })
    }


}

module.exports = PersonaController;