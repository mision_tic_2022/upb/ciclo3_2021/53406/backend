//Importar express
const express = require('express');
const PersonaRouter = require('./routers/personaRouter');
const mongoose = require('mongoose');
const key_database = require('./database/key_database');

class Server{
    //Método constructor
    constructor(){
        //Generar conexión a la BD
        this.conectarBD();
        //Crear aplicación express
        this.app = express();
        this.config();
    }

    config(){
        //Indicar en una variable de express el puerto por el que correrá el servidor
        this.app.set('port', process.env.PORT || 3000);
        //Indicar que en las peticiones se procesarán datos en formato json
        this.app.use(express.json());
        //Crear objeto router
        let router = express.Router();
        //Crear ruta raíz del servidor
        router.get('/', (req, res)=>{
            res.status(200).json({"messages": "All ok"});
        });
        //CREAR OBJETOS ROUTERS
        const objPersonaR = new PersonaRouter();
        /*********AÑADIR RUTAS A EXPRESS********/
        this.app.use(router);
        this.app.use(objPersonaR.router);
        //Levantar el servidor / poner el servidor a la escucha
        this.app.listen( this.app.get('port'), ()=>{
            console.log("Servidor corriendo por el puerto => ", this.app.get('port'));
        } );
    }

    conectarBD(){
        mongoose.connect(key_database.db).then(()=>{
            console.log("Conexión exitosa a la BD");
        }).catch(error=>{
            console.log(error);
        });
    }
}

const obj = new Server();