const mongoose = require('mongoose');

const Schema = mongoose.Schema;

//Construir el schema para el modelo persona
let personaSchema = new Schema({
    nombre: {
        type: String
    },
    apellido: {
        type: String
    },
    edad: {
        type: Number
    }
}, {
    collection: 'personas'
});

module.exports = mongoose.model('Persona', personaSchema);