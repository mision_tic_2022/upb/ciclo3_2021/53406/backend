//Importar express
const express = require('express');
const PersonaController = require('../controllers/personaController');

class PersonaRouter {
    constructor(){
        this.router = express.Router();
        this.config();
    }

    config(){
        const objPersonaC = new PersonaController();
        
        this.router.get('/personas', objPersonaC.getPersonas);
        this.router.get('/personas/:apellido', objPersonaC.getPersonaApellido);
        this.router.post('/personas', objPersonaC.crearPersona);
        this.router.put('/personas', objPersonaC.actualizarPersona);
        this.router.delete('/personas', objPersonaC.eliminarPersona);
    }
}

module.exports = PersonaRouter;